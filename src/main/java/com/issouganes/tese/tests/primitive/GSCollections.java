package com.issouganes.tese.tests.primitive;

import com.gs.collections.impl.list.mutable.primitive.IntArrayList;
import com.issouganes.tese.SortHelper;

import java.util.Collections;
import java.util.Random;

public class GSCollections {
    public static void main( String[] args )
    {
        Random random = new Random();
        IntArrayList list = new IntArrayList(SortHelper.SIZE);

        for (int i = 0; i < SortHelper.SIZE; i++) {
            list.add(random.nextInt());
        }

        list.sortThis();
    }
}
