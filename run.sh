./mvnw compile
rm -rf results
mkdir -p results

## boxede
# fastutil
perf record -g -k mono java -cp ~/projects/tese/target/classes -XX:+PreserveFramePointer -agentpath:/home/basto/lib64/libperf-jvmti.so com/issougames/tests/boxed/FastUtil
perf inject --jit -i perf.data > results/fastutil-boxed.data.jitted
perf script -i results/fastutil-boxed.data.jitted | ~/FlameGraph/stackcollapse-perf.pl > results/fastutil-boxed.perf-folded
~/FlameGraph/flamegraph.pl results/fastutil-boxed.perf-folded > results/fastutil-boxed.flamgraph.svg

# gscollection
perf record -g -k mono java -cp ~/projects/tese/target/classes -XX:+PreserveFramePointer -agentpath:/home/basto/lib64/libperf-jvmti.so com/issougames/tests/boxed/GSCollection
perf inject --jit -i perf.data > results/gscollection-boxed.data.jitted
perf script -i results/gscollection-boxed.data.jitted | ~/FlameGraph/stackcollapse-perf.pl > results/gscollection-boxed.perf-folded
~/FlameGraph/flamegraph.pl results/gscollection-boxed.perf-folded > results/gscollection-boxed.flamgraph.svg

#JCF
perf record -g -k mono java -cp ~/projects/tese/target/classes -XX:+PreserveFramePointer -agentpath:/home/basto/lib64/libperf-jvmti.so com/issougames/tests/boxed/JCF
perf inject --jit -i perf.data > results/jcf-boxed.data.jitted
perf script -i results/jcf-boxed.data.jitted | ~/FlameGraph/stackcollapse-perf.pl > results/jcf-boxed.perf-folded
~/FlameGraph/flamegraph.pl results/jcf-boxed.perf-folded > results/jcf-boxed.flamgraph.svg


##primitive

# gscollection
perf record -g -k mono java -cp ~/projects/tese/target/classes -XX:+PreserveFramePointer -agentpath:/home/basto/lib64/libperf-jvmti.so com/issougames/tests/primitive/GSCollection
perf inject --jit -i perf.data > results/gscollection-primitive.data.jitted
perf script -i results/gscollection-primitive.data.jitted | ~/FlameGraph/stackcollapse-perf.pl > results/gscollection-primitive.perf-folded
~/FlameGraph/flamegraph.pl results/gscollection-primitive.perf-folded > results/gscollection-primitive.flamgraph.svg

# fastutil
perf record -g -k mono java -cp ~/projects/tese/target/classes -XX:+PreserveFramePointer -agentpath:/home/basto/lib64/libperf-jvmti.so com/issougames/tests/primitive/FastUtil
perf inject --jit -i perf.data > results/fastutil-primitive.data.jitted
perf script -i results/fastutil-primitive.data.jitted | ~/FlameGraph/stackcollapse-perf.pl > results/fastutil-primitive.perf-folded
~/FlameGraph/flamegraph.pl results/fastutil-primitive.perf-folded > results/fastutil-primitive.flamgraph.svg

# JCF
perf record -g -k mono java -cp ~/projects/tese/target/classes -XX:+PreserveFramePointer -agentpath:/home/basto/lib64/libperf-jvmti.so com/issougames/tests/primitive/JCF
perf inject --jit -i perf.data > results/jcf-primitive.data.jitted
perf script -i results/jcf-primitive.data.jitted | ~/FlameGraph/stackcollapse-perf.pl > results/jcf-primitive.perf-folded
~/FlameGraph/flamegraph.pl results/jcf-primitive.perf-folded > results/jcf-primitive.flamgraph.svg

# trove
perf record -g -k mono java -cp ~/projects/tese/target/classes -XX:+PreserveFramePointer -agentpath:/home/basto/lib64/libperf-jvmti.so com/issougames/tests/primitive/Trove
perf inject --jit -i perf.data > results/trove-primitive.data.jitted
perf script -i results/trove-primitive.data.jitted | ~/FlameGraph/stackcollapse-perf.pl > results/trove-primitive.perf-folded
~/FlameGraph/flamegraph.pl results/trove-primitive.perf-folded > results/trove-primitive.flamgraph.svg

rm perf.data*


echo "rm -rf ~/desktop/results/lists && mkdir -p ~/desktop/results && scp -r basto@192.168.0.100:/home/basto/tese/results ~/desktop/results/lists"