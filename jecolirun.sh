./mvnw clean compile compiler:testCompile
rm -rf results
mkdir -p results

#caga
perf record -g -k mono java -cp target/classes:target/test-classes -XX:+PreserveFramePointer -agentpath:/home/basto/lib64/libperf-jvmti.so pt.uminho.ceb.biosystems.jecoli.demos.countones.CountOnesCAGATest
perf inject --jit -i perf.data > results/countonesCagaTest.data.jitted
perf script -i results/countonesCagaTest.data.jitted | ~/FlameGraph/stackcollapse-perf.pl > results/countonesCagaTest.perf-folded
~/FlameGraph/flamegraph.pl results/countonesCagaTest.perf-folded > results/countonesCagaTest.flamgraph.svg

#ea
perf record -g -k mono java -cp target/classes:target/test-classes -XX:+PreserveFramePointer -agentpath:/home/basto/lib64/libperf-jvmti.so pt.uminho.ceb.biosystems.jecoli.demos.countones.CountOnesEATest
perf inject --jit -i perf.data > results/countonesEaTest.data.jitted
perf script -i results/countonesEaTest.data.jitted | ~/FlameGraph/stackcollapse-perf.pl > results/countonesEaTest.perf-folded
~/FlameGraph/flamegraph.pl results/countonesEaTest.perf-folded > results/countonesEaTest.flamgraph.svg

rm perf.data*


echo "rm -rf ~/desktop/results/jecoli && scp -r basto@192.168.0.100:/home/basto/jecoli/results ~/desktop/results/jecoli"